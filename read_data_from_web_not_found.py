import requests
from bs4 import BeautifulSoup

url = "https://www.to-paints.com/product/3"
web_data = requests.get(url)

soup = BeautifulSoup(web_data.text,'html.parser')
product_not_found = soup.find_all("div",{"class":"not_found"})[0]

if '404 Product not found.' == product_not_found.text:
    print("continue")
else:
    print("save data")



