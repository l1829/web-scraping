import requests
from bs4 import BeautifulSoup

new_path_products = 'new_products.sql'
new_products = open(new_path_products,'w')
new_products.write('''

''')

rows_products = []
rows_images = []

counter_position = 1

for index in range(3,2530):

    url = "https://www.to-paints.com/product/{}".format(index)
    web_data = requests.get(url)
    soup = BeautifulSoup(web_data.text,'html.parser')

    product_not_found = soup.find_all("div",{"class":"not_found"})

    if len(product_not_found) == 1:
        print("continue")
    else:
        print("save data")

        short_description = soup.find_all("div",{"class":"short_description"})
        if len(short_description)>0:
            short_description = soup.find_all("div",{"class":"short_description"})[0]
            short_description = short_description.text
        else:
            print('else short_description')
            print(short_description)
            continue

        price = soup.find_all("td",{"class":"bodyTD"})[1].text
        last_text = len(price)
        price = price.replace(",", "")
        price = float(price[:last_text-4])

        product_image = soup.find_all("img",{"class":"productImage"})[0]
        product_image = product_image.attrs['src']
        detail_all = soup.find_all("div",{"id":"detail"})[0]
        detail_all = str(detail_all)
        detail_all = detail_all.replace("'","\\\'")

        print('counter_position ',counter_position)
        print(short_description)
        print(price)
        print(product_image)
        # print(detail_all)

        new_product_item = '''
        INSERT INTO `products` (`product_name`,`price`,`detail`) 
        VALUES  ('{}',{},'{}' );
        '''.format(short_description,price,detail_all)
        rows_products.append(new_product_item)

        response = requests.get(product_image)

        file = open("uploads/{}-image00.jpg".format(counter_position), "wb")

        file.write(response.content)

        file.close()

        new_image_item = '''
        INSERT INTO `images` (`product_id`,`name`) 
        VALUES  ( {},'{}' );'''.format(counter_position,"{}-image00.jpg".format(counter_position))
        rows_products.append(new_image_item)

        counter_position = counter_position + 1

        # break

# Writing multiple strings
# at a time
new_products.writelines(rows_products)
  
# Closing file
new_products.close()
